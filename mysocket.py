import socket
import time

def send_message(socket, msg):
    total_bytes_written = 0
    msg_len = len(msg)
    while total_bytes_written < msg_len:
        bytes_written = socket.send(msg)
        if bytes_written == 0:
            return False
        total_bytes_written += bytes_written
    return socket.send("\n") == 1


def receive_message(socket, timed=False):
    message = ''
    start_time = time.time()
    download_time = 0
    while True:
        chunk = socket.recv(1024)
        if chunk == '':
            return 0
        message += chunk
        if message[-1] == "\n":
            download_time = time.time() - start_time
            break
    if timed:
        return message[:-1], download_time
    else:
        return message[:-1]
