#!/usr/bin/python
import os 
import sys
import socket
import threading
import time
from mysocket import send_message, receive_message

from argparse import ArgumentParser

class Server:
    def __init__(self, debug=False):
        self.debug = debug

    def run_forever(self):
        self.debug_print('server hello')
        serversocket = socket.socket()
        serversocket.bind(('0.0.0.0', 8000))
        serversocket.listen(5)
        (self.clientsocket, addr) = serversocket.accept()

        while True:
            response_length = int(receive_message(self.clientsocket)) 
            self.debug_print('server received request')
            if response_length == 0:
                print "something is wrong?"
                break
            else:
                send_message(self.clientsocket, "-" * response_length)
                self.debug_print('server sent response length %d' % response_length)

    def debug_print(self, msg):
        if self.debug:
            print msg
            sys.stdout.flush()

if __name__ == '__main__':
    try:
        Server(debug=False).run_forever()
    except Exception as e:
        print '----- error in server -----'
        print e
        import traceback
        traceback.print_exc()
        sys.stdout.flush()
