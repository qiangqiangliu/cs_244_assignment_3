import os
from helper import *
from matplotlib.font_manager import FontProperties

parser = argparse.ArgumentParser()
parser.add_argument('--files', '-f',
                    help="Rate timeseries output to one plot",
                    required=True,
                    action="store",
                    nargs='+',
                    dest="files")

parser.add_argument('--legend', '-l',
                    help="Legend to use if there are multiple plots.  File names used as default.",
                    action="store",
                    nargs="+",
                    default=None,
                    dest="legend")

parser.add_argument('--out', '-o',
                    help="Output png file for the plot.",
                    default=None, # Will show the plot
                    dest="out")

parser.add_argument('-s', '--summarise',
                    help="Summarise the time series plot (boxplot).  First 10 and last 10 values are ignored.",
                    default=False,
                    dest="summarise",
                    action="store_true")

parser.add_argument('--labels',
                    help="Labels for x-axis if summarising; defaults to file names",
                    required=False,
                    default=[],
                    nargs="+",
                    dest="labels")

parser.add_argument('--xlabel',
                    help="Custom label for x-axis",
                    required=False,
                    default=None,
                    dest="xlabel")

parser.add_argument('--ylabel',
                    help="Custom label for y-axis",
                    required=False,
                    default=None,
                    dest="ylabel")

parser.add_argument('-i',
                    help="Interfaces to plot (regex)",
                    default=".*",
                    dest="pat_iface")

parser.add_argument('--rx',
                    help="Plot receive rates on the interfaces.",
                    default=False,
                    action="store_true",
                    dest="rx")

parser.add_argument('--maxy',
                    help="Max mbps on y-axis..",
                    default=100,
                    action="store",
                    dest="maxy")

parser.add_argument('--miny',
                    help="Min mbps on y-axis..",
                    default=0,
                    action="store",
                    dest="miny")

parser.add_argument('--normalize',
                    help="normalise y-axis",
                    default=False,
                    action="store_true",
                    dest="normalise")

parser.add_argument('--start', dest="start", default=None, type=int)
parser.add_argument('--length', dest="length", default=None, type=int)

args = parser.parse_args()
if args.labels is None:
    args.labels = args.files

pat_iface = re.compile(args.pat_iface)

to_plot=[]
"""Output of bwm-ng csv has the following columns:
unix_timestamp;iface_name;bytes_out;throughput;bytes_total;packets_out;packets_in;packets_total;errors_out;errors_in
"""

if args.normalise and args.labels == []:
    raise "Labels required if summarising/normalising."
    sys.exit(-1)

bw = map(lambda e: int(e.replace('M','')), args.labels)
idx = 0

rate = {}
for f in args.files:
    if not os.path.exists(f):
        continue
    data = read_list(f)
    #xaxis = map(float, col(0, data))
    #start_time = xaxis[0]
    #xaxis = map(lambda x: x - start_time, xaxis)
    #rate = map(float, col(2, data))
    column = 2
    if args.rx:
        column = 3
    for row in data:
        try:
            ifname = row[1]
        except:
            break
        if ifname not in ['eth0', 'lo']:
            x = float(row[0])
            y = float(row[column]) / (1000)
            if not rate.has_key(ifname):
                rate[ifname] = ([], [],)
            try:
                rate[ifname][0].append(x)
                rate[ifname][1].append(y)
            except Exception as e:
                print e
                break

earliest_time = -1 
for k in sorted(rate.keys()):
    times = rate[k][0]
    if earliest_time == -1 or times[0] < earliest_time:
        earliest_time = times[0]
'''
UGH why doesn't this work
if args.start and args.length:
    for k in sorted(rate.keys()):
        data = rate[k]
        new_data = []
        for i in range(len(data[0])):
            if data[0][i]-earliest_time > args.start and data[0][i]-earliest_time) < (args.start + args.length):
                new_data.append([data[0][i], data[1][i]])
        rate[k] = new_data
    print new_data
'''

for k in sorted(rate.keys()):
    data = rate[k]
    times = rate[k][0]
    time_offsets = [t - earliest_time for t in times]
    values = rate[k][1]

    if pat_iface.match(k):
        print k
        if k == "video-flow-throughput": 
            plt.plot(time_offsets, values, label=k, marker='x', color='b')
        elif k == "video-playback-rate": 
            plt.plot(time_offsets, values, label=k, marker='+', color='r')
        elif k == "competing-throughput": 
            plt.plot(time_offsets, values, 'k--', label=k, color='g')
        else: 
            plt.plot(time_offsets, values, label=k, color='y')

plt.title("TX rates")
if args.rx:
    plt.title("RX rates")

if args.ylabel:
    plt.ylabel(args.ylabel)
elif args.normalise:
    plt.ylabel("Normalized BW")
else:
    plt.ylabel("kbps")

plt.grid()
fontP = FontProperties()
fontP.set_size('small')
plt.legend(prop = fontP, bbox_to_anchor=(0., 1.05, 1., .102), loc=3,
           ncol=3, mode="expand", borderaxespad=0.)
plt.ylim((int(args.miny), int(args.maxy)))
if args.summarise:
    plt.boxplot(to_plot)
    plt.xticks(range(1, 1+len(args.files)), args.labels)

if not args.summarise:
    if args.xlabel:
        plt.xlabel(args.xlabel)
    else:
        plt.xlabel("Time")
    if args.legend:
        plt.legend(args.legend)

if args.out:
    plt.savefig(args.out)
else:
    plt.show()


