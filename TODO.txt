Make a streaming client
    Socket Communication
    Logic for when to request files
    Get available bandwidth estimates
    Video playback rate as a function of available bandwidth
        see figure 9, should be easy given that
        Also see text under Figure 20, suggests ten samples
Make a streaming server
    Socket Communication
    Generate files
Make topology
    Bandwidth limited to 5Mbps, buffer size is 120kbit, 4-20 ms RTT
