#!/usr/bin/python

import threading
import time
import sys

from extras import MovingRecord
from argparse import ArgumentParser

def debug_print(msg):
    print msg
    sys.stdout.flush()

def get_rxbytes(iface):
    f = open('/proc/net/dev', 'r')
    lines = f.readlines()
    for line in lines:
        if iface in line:
            break
    f.close()
    if not line:
        raise Exception("could not find iface %s in /proc/net/dev:%s" %
                        (iface, lines))
    # Extract TX bytes from:
    #Inter-|   Receive                                                |  Transmit
    # face |bytes    packets errs drop fifo frame compressed multicast|bytes    packets errs drop fifo colls carrier compressed
    # lo: 6175728   53444    0    0    0     0          0         0  6175728   53444    0    0    0     0       0          0
    return float(line.split()[1])

def monitor_tcp_throughput(iface, fname, field_name):
    last_time = time.time()
    first_time = last_time
    last_rxbytes = get_rxbytes(iface)
    record = MovingRecord(1)
    while True:
        if args.gran:
            time.sleep(0.2)
        else:
            time.sleep(4.0)
        rxbytes = get_rxbytes(iface)
        current_time = time.time()
        throughput = (rxbytes-last_rxbytes)/(current_time-last_time)
        record.add_sample(throughput)
        last_time = current_time
        last_rxbytes = rxbytes
        f = open(fname, 'a')
        f.write("%f,%s,0,%d,0,0,0,0,0\n" % 
            (current_time, field_name, record.average()*8))
        f.close()

def parse_args():
    parser = ArgumentParser(description="Monitor TCP throughput")

    parser.add_argument('--interface',
                        action='store',
                        help='Interface to monitor',
                        required=True)

    parser.add_argument('--fname', '-f',
                        action='store',
                        help='File to write',
                        required=True)

    parser.add_argument('--field-name',
                        action='store',
                        help='Field name for the graph',
                        required=True)

    parser.add_argument('--gran',
                        action='store_true',
                        help='Field name for the graph',
                        default=False)

    return parser.parse_args()


if __name__ == '__main__':
    try:
        args = parse_args()
        monitor_tcp_throughput(args.interface, args.fname, args.field_name)
    except Exception as e:
        print '----- error in tcp monitoring -----'
        print e
        sys.stdout.flush()
        sys.stderr.flush()
