#!/bin/bash

# Exit on any failure
set -e

# Check for uninitialized variables
set -o nounset

ctrlc() {
	killall -9 python
	mn -c
	exit
}

trap ctrlc SIGINT

start=`date`
exptid=`date +%b%d-%H:%M`

rootdir=rate-$exptid
plotpath=util
iperf=~/iperf-patched/src/iperf

# TODO: change the interface name for which queue size is adjusted
# Links are numbered as switchname-eth1,2,etc in the order they are
# added to the topology.
iface=s1-eth3

if [ $# -eq 1 ]; then
    test=$1
else
    test="none"
fi

for run in 1; do
    dir=$rootdir

    ./rate.py --maxq 10 --dir $dir -t 1300 -c 300 -p 240 -r 900 --force 450 --test $test # 120 * 10^3 bit / (1500 bytes * 8 bits/byte) = 10 packets 

     if [ $test = "none" ]; then
         echo "Figure 20 in rate.png"
         python $plotpath/plot_rates.py -f $dir/video_rate.txt $dir/competing_throughput.txt -o $dir/rate.png --rx --miny 0 --maxy 5000
         echo "Figure 6a in tcp_rate.png"
         python $plotpath/plot_rates.py -f $dir/tcp_rate.txt -o $dir/tcp_rate.png --rx --miny 0 --maxy 5000 --start 160 --length 50
         echo "Figure 8a in cwnd.png"
         python $plotpath/plot_tcpprobe.py -f $dir/tcp_probe.txt -o $dir/cwnd.png -p 8000 --sport -s 600 -l 3
         echo "Figure 6b in request-interval.png"
         python $plotpath/plot_request_interval.py -f $dir/request_interval.txt   -o $dir/request-interval.png  --miny 0 --maxy 10
     elif [ $test = "dl" ]; then
         echo "Hit Ctrl-C to exit"
     elif [ $test = "force" ]; then
        echo "Figure 5 in rate.png"
        python $plotpath/plot_rates.py -f $dir/video_rate.txt $dir/competing_throughput.txt -o $dir/rate.png --rx --miny 0 --maxy 5000
    fi
done
